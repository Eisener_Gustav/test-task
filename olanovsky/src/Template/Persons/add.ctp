

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Persons'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="persons form large-9 medium-8 columns content">
    <?= $this->Form->create($person) ?>
    <fieldset>
        <legend><?= __('Add Person') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('password');
            //echo $this->Form->input('gender');
            echo $this->Form->input('gender', array('options' => array('M' => 'Male', 'F' => 'Female')));
            echo $this->Form->input('email');
        ?>
    </fieldset>
    <!-- ?= $this->Form->button(__('Submit')) ? -->
    <button type="button" id="submit_button">Submit</button>
    <?= $this->Form->end() ?>
</div>
<script>
$('#submit_button').click(function(){
    //validating the input
    var re = /\S+@\S+\.\S+/;
    if ($('#name').val() == ''){
        alert('Name cannot be empty!');
        return false;
    }
    if ($('#password').val() == ''){
        alert('Password cannot be empty!');
        return false;
    }
    if (!re.test($('#email').val())){
        alert('Please input valid email');
        return false;
    }
    $(this).parent().submit();
});

</script>
