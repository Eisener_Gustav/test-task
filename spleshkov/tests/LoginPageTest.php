<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginPageAvailability()
    {
        $response = $this->call('GET', '/registration/login');

        $this->assertEquals(200, $response->status());
    }
}
