@extends('layouts.main')

@section('content')
    <div class="row">
        <h1>Login</h1>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li> @endforeach
        </ul>

        @if(Session::has('message'))
            <div class="alert alert-info">
                {{ Session::get('message') }}
            </div>
        @endif

        <div class="message"></div>
        <div class="col-md-4 col-md-offset-4">
            <form id="registrationForm" method="post" action="/registration/login">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
                <button type="submit" class="btn btn-default" id="formSubmit">Submit</button>
            </form>
        </div>
    </div>
@stop
@section('scripts')

@stop
